import json
from tracemalloc import start
from turtle import end_fill
from venv import create
from model import Year, Month, Day, Activity
import datetime

class Database:

    def __init__(self):
        self.years = {}
        self.filename = 'database.json'

    def load(self): 
        with open(self.filename, 'r', encoding='utf-8') as fp:
            years_dict = json.load(fp)
        if years_dict.get('years'):
            for year, year_obj in years_dict['years'].items():
                self.years[year] = Year.from_dict(year_obj)

    def save(self): # Should pull and reload before running this
        years = {'years': {}}
        for year in self.years:
            years['years'][year.year] = year.to_json()
        with open(self.filename, 'w', encoding='utf-8') as fp:
            json.dump(years, fp)
        
    def read_all(self):
        return self.years

    def read(self, cls, date, id): 
        '''Finds cls by id, returns None if not found'''
        if cls.lower() == 'activity':
            return self.read_activity(date, id)
        else:
            return eval('read_{}'.format(cls.lower()))(date)
    def read_activity(self, date):
        day = self.read_day(date)
        hour = date.hour
        minute = date.minute
        key = '{}:{}'.format(hour, minute)
        if not day: return None
        return day.activities.get(key)
    def read_day(self, date): 
        month = self.read_month(date)
        if not month: return None
        return month.get(date.day)
    def read_month(self, date):
        year = self.read_year(date.year)
        if not year: return None
        return year.months.get(date.month)
    def read_year(self, date): return self.years.get(date.year)

    def create(self, cls, date, data_obj):
        '''if the class and date already exist, this does nothing
            data_obj should be:
            activity -> {'start':datetime, 'end':datetime, 'description':str, 'rank':str}'
            year -> {date} can be inferred from the 'start' datetime if not present
            month -> {date}
            day -> {date} 
            If year, month, or day must be created, then the parent element must add them'''
        if cls == 'Activity':
          return self.create_activity(data_obj)

    def create_activity(self, data_obj):
        start = data_obj.get('start')
        end = data_obj.get('end')
        desc = data_obj.get('description')
        rank = data_obj.get('rank')
        act = Activity(start, end, desc, rank)
        day = self.create_day(data_obj)

    def create_day(self,data_obj):
        month = self.create_month(data_obj)
        date = data_obj.get('date')
        if not date: date = data_obj.get('start')
        if month.get(date.day): return month.get(date.day)
        else:
            date = datetime.date(date.year, date.month, date.day)
            month.add_day(Day(date))
            return month.days[date.day]

    def create_month(self,data_obj):
        year = self.create_year(data_obj)
        date = data_obj.get('date')
        if not date: date = data_obj.get('start')
        if year.months.get(date.month): return year.months.get(date.month)
        else:
            date = datetime.date(date.year, date.month, 1)
            year.add_month(Month(date))
            return year.months[date.month]

    def create_year(self,data_obj):
        date = data_obj.get('date')
        if not date: date = data_obj.get('start')
        date = datetime.date(date.year, 1, 1)
        date_int = date.year
        if self.years.get(date_int): return self.years.get(date_int)
        else:
            self.years[date_int] = Year(date)
            return self.years[date_int]

    def update_activity(self, date, update_dict):
        act = self.read_activity(date)
        if not act: return None
        updater = {}
        for key, val in update_dict.items():
            if key in ['start', 'end', 'description', 'rank', 'id']: updater[key] = val
        act.update(**updater)
            
    def delete_activity(self, date):
        '''We need the year, month, day, hour, and minute'''
        day = self.read_day(date)
        if not day: return None
        hour = date.hour
        minute = date.minute
        key = '{}:{}'.format(hour, minute)
        act = day.activities.get(key)
        if not act: return None
        else:
            return day.activities.pop(key)
