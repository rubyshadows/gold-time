import uuid

class Time:
    def __init__(self, start, end, id): # start and end must be datetime.datetime objects
        self.id = id
        self.start = start
        self.end = end
        seconds = self.end - self.start
        seconds = seconds.total_seconds()
        self.minutes = self.seconds // 60

    @staticmethod
    def add_times(time1, time2): pass

class Activity(Time):
    def __init__(self, start, end, description, rank, id=uuid.uuid4()):
        super().__init__(start, end, id)
        self.description = description
        self.rank = rank

    @staticmethod
    def from_dict(a): return Activity(a['start'], a['end'], a['description'], a['rank'], a['id'])

    def update(self, **kwargs):
        if not (kwargs): pass
        else:
            for key, value in kwargs.items():
                if key in ['start', 'end', 'description', 'rank', 'id']: 
                    setattr(self, key, value)
                    return self

    def to_json(self):
        rep = {'id': self.id}
        rep['description'] = self.description
        rep['rank'] = self.rank
        rep['start'] = self.start
        rep['end'] = self.end
        rep['minutes'] = self.minutes
        return rep

class Day: 
    def __init__(self, date, id=uuid.uuid4()): # date should be a datetime.date object
        self.id = id
        self.date = date
        self.activities = {}

    def add_activity(self, act): 
        start_hour = act.start.hour
        start_minute = act.start.minute
        index = '{}:{}'.format(str(start_hour), str(start_minute))
        self.activities[index] = act

    def remove_activity(self, id):
        return self.activities.pop(id)

    def update_activity(self, act): self.activities[act.id] = act
    def delete_activity(self, id): self.activities.pop(id) 

    def get_rank(self, rank): # rank must be a string
        minutes = 0
        for act in self.activities.values:
            if act.rank.lower() == rank.lower():
                minutes += act.minutes
        return minutes

    @property
    def gold(self): return self.get_rank('gold')
    @property
    def silver(self): return self.get_rank('silver')
    @property
    def bronze(self): return self.get_rank('bronze')
    @property
    def rank(self):
        bronze = self.bronze
        silver = self.silver
        gold = self.gold
        if bronze > silver and bronze > gold: return 'Bronze'
        elif gold > silver > bronze: return 'Gold'
        else: return 'Silver'

    @staticmethod
    def from_dict(day_dict):
        date = day_dict.get('day_date')
        id = day_dict.get('id')
        day = Day(date, id)
        acts = day_dict.get('activities')
        for act in acts:
            activity = Activity.from_dict(act)
            day.add_activity(activity)
        return day

    def to_json(self):
        rep = {
            'id': self.id,
            'day_date': self.date, 
            'activities': {},
            'rank': self.rank,
            'gold': self.gold,
            'silver': self.silver,
            'bronze': self.bronze}
        for act in self.activities:
            rep['activities'][act] = self.activities.get(act).to_json()
        return rep

class Month: 
    def __init__(self, date, id=uuid.uuid4()): # date should be a datetime.date object
        self.id = id
        self.date = date
        self.days = {}

    def add_day(self, day): self.days[day.date.day] = day

    def get_rank(self, rank): # rank must be a string
        days = 0
        for day in self.days.values:
            if day.rank.lower() == rank.lower():
                days += 1
        return days

    @property
    def gold(self): return self.get_rank('gold')
    @property
    def silver(self): return self.get_rank('silver')
    @property
    def bronze(self): return self.get_rank('bronze')
    @property
    def rank(self):
        bronze = self.bronze
        silver = self.silver
        gold = self.gold
        if bronze > silver and bronze > gold: return 'Bronze'
        elif gold > silver > bronze: return 'Gold'
        else: return 'Silver'

    @staticmethod
    def from_dict(month_dict): 
        id = month_dict.get('id')
        date = month_dict.get('month_date')
        month = Month(date, id)
        days = month_dict.get('days')
        for day_dict in days:
            day = Day.from_dict(day_dict)
            month.add_day(day)
        return month

    def to_json(self):
        rep = {'month_date': self.date, 'days': {}, 'id': self.id,
                'rank': self.rank,
                'gold': self.gold,
                'silver': self.silver,
                'bronze': self.bronze}
        for day in self.days:
            rep['days'][day] = self.days.get(day).to_json()
        return rep

class Year:
    def __init__(self, date, id=uuid.uuid4()): # date should be a datetime.date object
        self.id = id
        self.date = date
        self.months = {}

    def add_month(self, month): self.months[month.date.month] = month

    def get_rank(self, rank): # rank must be a string
        months = 0
        for month in self.months.values:
            if month.rank.lower() == rank.lower():
                months += 1
        return months

    @property
    def gold(self): return self.get_rank('gold')
    @property
    def silver(self): return self.get_rank('silver')
    @property
    def bronze(self): return self.get_rank('bronze')
    @property
    def rank(self):
        bronze = self.bronze
        silver = self.silver
        gold = self.gold
        if bronze > silver and bronze > gold: return 'Bronze'
        elif gold > silver > bronze: return 'Gold'
        else: return 'Silver'

    @staticmethod
    def from_dict(year_dict):
        id = year.dict.get('id')
        date = year_dict.get('year_date')
        year = Year(date, id)
        months = year_dict.get('months')
        for month_dict in months:
            month = Month.from_dict(month_dict)
            year.add_month(month)
        return year

    def to_json(self): 
        rep = {'year_date': self.date, 'months': {}, 'id': self.id,
                'rank': self.rank,
                'gold': self.gold,
                'silver': self.silver,
                'bronze': self.bronze}
        for month in self.months:
            rep['months'][month] = self.months.get(month).to_json()
        return rep